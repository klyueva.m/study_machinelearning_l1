import time
import datetime
import pandas as pd
from sklearn.model_selection import KFold
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler

#______________Подход 1: градиентный бустинг "в лоб"______________
#____________________________Задание 1____________________________
#Считывание обучающей выборки и тестовой
features = pd.read_csv('data/features.csv', index_col='match_id')
#Целевая переменная
y_train = features['radiant_win']
features_test = pd.read_csv('data/features_test.csv',index_col='match_id')
#Удаление признаков, которые связаны с итогами матча
features_difference = features.columns.difference(features_test.columns.values.tolist()).tolist()
features.drop(features_difference, axis=1, inplace=True)
#____________________________Задание 2____________________________
#Проверка выборки на наличие пропусков
features_size = features.shape[0]
print ('Names of features that have skips')
for c in features.columns.values.tolist():
    count = features[c].count()
    if count != features_size:
    	print ('Feature %s, skip: %s' % (c, (features_size - count)))
#____________________________Задание 3____________________________
#Замена пропусков на нули
features.fillna(0, method=None, axis=1, inplace=True)
#____________________________Задание 4____________________________
#Целевая переменная: 1 - победила команда Radiant, 0 — победила команда Dire
print ('Target variable: radiant_win')
#____________________________Задание 5____________________________
#Стандартные значения
#Конструктор кросс-валидации
kf = KFold(n_splits=5, shuffle=True)
for e in range(10,31,10):
	#Конструктор классификатора
    clf = GradientBoostingClassifier(n_estimators=e, random_state=241)
    #star_time
    start_time = datetime.datetime.now()
    #Оценка алгоритма
    scores = cross_val_score(clf, features, y_train, scoring='roc_auc', cv=kf)
    #stop_time
    print('Time elapsed:', datetime.datetime.now() - start_time)
    #Среднее значение оценки
    scores_mean=round(scores.mean()*100,2)
    print('n_estimators = %s, scores_mean = %s' % (e,scores_mean))
#Уменьшается глубина дерева и ускоряется процесс
#Параметры сетки тестирования алгоритма
param_grid_gb  = {'n_estimators':[50,70],'max_depth': range(2,4),'max_features': ["log2"]}
clf_grid_gb = GridSearchCV(GradientBoostingClassifier(random_state=241), param_grid_gb,cv=kf, n_jobs=1,verbose=1,scoring='roc_auc')
clf_grid_gb.fit(features, y_train)
#Конструктор классификатора с наилучшими параметрами
clf=GradientBoostingClassifier(random_state=241,**clf_grid_gb.best_params_)
#star_time
start_time = datetime.datetime.now()
#Оценка алгоритма
scores = cross_val_score(clf, features, y_train, scoring='roc_auc', cv=kf)
#stop_time
print('Time elapsed:', datetime.datetime.now() - start_time)
#Среднее значение оценки
scores_mean = round(scores.mean()*100,2)
print('best_params: %s, scores_mean = %s' % (clf_grid_gb.best_params_, scores_mean))
#______________Подход 2: логистическая регрессия______________
#__________________________Задание 1__________________________
#Оценка качества логистической регрессии 
#Параметры сетки тестирования алгоритма - логарифмическая
param_grid_lr  = {'C': np.logspace(-3, -1, 10)}
def getScoreLogisticRegression(text, x_data, y_train_=y_train):
    clf_grid_lr = GridSearchCV(LogisticRegression(random_state=241,n_jobs=-1), param_grid_lr,cv=kf, n_jobs=1,verbose=1,scoring='roc_auc')
    clf_grid_lr.fit(x_data, y_train_)
    #Создается логистрическая регрессия с лучшими параметрами
    lr=LogisticRegression(n_jobs=-1,random_state=241,**clf_grid_lr.best_params_)
    lr.fit(x_data, y_train_)
    #star_time
    start_time = datetime.datetime.now()
    #Оценка алгоритма
    scores = cross_val_score(lr, x_data, y_train_, scoring='roc_auc', cv=kf)
    #stop_time
    print('Time elapsed:', datetime.datetime.now() - start_time)
    #Среднее значение оценки
    scores_mean=round(scores.mean()*100,2)
    print(text)
    print('best_params: %s, scores_mean = %s' % (clf_grid_lr.best_params_, scores_mean))
    #__________________________Задание 6__________________________
    #Прогнозирование
    y_pred = pd.DataFrame(data=lr.predict_proba(x_data.iloc))
    #Сортировка
    y_pred.sort_values([1],inplace=True)
    #1 - класс означает, что Radiant победил
    print('min=',y_pred.iloc[0,1],'; max=',y_pred.iloc[y_pred.shape[0]-1,1])
#Оценка качества логистической регрессии
getScoreLogisticRegression('LogisticRegression',features, y_train)
#__________________________Задание 2__________________________
#Удаление категориальных признаков
cols = ['r%s_hero' % i for i in range(1, 6)]+['d%s_hero' % i for i in range(1, 6)]
cols.append('lobby_type')
features_norm = pd.DataFrame(data=StandardScaler().fit_transform(features.drop(cols, axis=1)))
#Проведится кросс-валидация для логистической регрессии на новой выборке с подбором лучшего параметра регуляризации
getScoreLogisticRegression('drop categories',features, y_train)
#__________________________Задание 3__________________________
#Убирается из списка лишний столбец
cols.remove('lobby_type')
cols_remove = pd.Series(features[cols].values.flatten()).drop_duplicates()
N = cols_remove.shape[0]
#Переводится в обычный массив, чтобы индексация была чистая
cols_remove = pd.DataFrame(data=list(range(N)),index=cols_remove.tolist())
cols_remove.sort_index(inplace=True)
print('Количество различных идентификаторов героев: ', N)
#__________________________Задание 4__________________________
#Подход "мешок слов" для кодирования информации о героях
X_pick = pd.DataFrame(index=features.index,columns=range(0,N))
for match_id in features.index:
	row = features.loc[match_id,cols]
	row_pick = X_pick.loc[match_id]
	for j, col in enumerate(row):
		row_pick[cols_remove.loc[col,0]] = 1 if j<5 else -1
X_pick.fillna(0, method=None, axis=1, inplace=True)
totalFeatures=features.join(X_pick,rsuffix='_',how='outer')
del X_pick,cols_remove
cols.append('lobby_type')
totalFeatures_norm=StandardScaler().fit_transform(totalFeatures.drop(cols, axis=1))
#__________________________Задание 5__________________________
#Проведится кросс-валидация для логистической регрессии на новой выборке с подбором лучшего параметра регуляризации
getScoreLogisticRegression('Bag of Words',pd.DataFrame(data=totalFeatures_norm))
