import pandas as pd
import numpy as np
import datetime

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler

def calc_roc_auc(x, y, c):
    kf = KFold(n_splits=5, random_state=1, shuffle=True)
    rnd = LogisticRegression(C=c, penalty='l2',  random_state=241)
    start_time = datetime.datetime.now()
    scores = cross_val_score(rnd, x, y, cv=kf, scoring='roc_auc')
    print 'Time elapsed:', datetime.datetime.now() - start_time
    print("Accuracy: %0.5f" % round(scores.mean(), 5))
    return scores.mean()

def calc_roc_auc_scale(x, y, c):
    scaler = StandardScaler()
    x = scaler.fit_transform(x)
    return calc_roc_auc(x, y, c)

def formBag(data, N):
    bag = np.zeros((data.shape[0], N))
    not_used_hero = [109, 108, 111]
    for i, match_id in enumerate(data.index):
        for p in xrange(5):
            if data.ix[match_id, 'r%d_hero' % (p + 1)] - 1 not in not_used_hero \
                    and data.ix[match_id, 'd%d_hero' % (p + 1)] - 1 not in not_used_hero:
                bag[i, data.ix[match_id, 'r%d_hero' % (p + 1)] - 1] = 1
                bag[i, data.ix[match_id, 'd%d_hero' % (p + 1)] - 1] = -1
    return bag

def calc_uniq_hero(df_heroes, hero_):
    result_hero = df_heroes['r1_hero']
    for hero in hero_:
        if hero!='match_id' and hero!='r1_hero':
            result_hero.append(df_heroes[hero])
    return result_hero.unique().size

def read_csv_with_scale_bag(filename):
    df = pd.read_csv(filename, index_col='match_id')
    df.drop(categorial_features, inplace=True, axis=1)
    df_heroes = pd.read_csv(filename, usecols=hero_, index_col='match_id');
    value_uniq = calc_uniq_hero(df_heroes, hero_)
    return np.hstack((df.apply(myfillna), formBag(df_heroes, value_uniq))) # Add word bag

def myfillna(series):
    if series.dtype is pd.np.dtype(float):
        return series.fillna(0)
    elif series.dtype is pd.np.dtype(object):
        return series.fillna('.')
    else:
        return series

df = pd.read_csv('./features.csv', index_col='match_id')
df.head()

#1 Fields with nan
count_nan_in_df = df.isnull().sum()
print (count_nan_in_df)
df = df.apply(myfillna)

#2 target field
y_train = df['radiant_win']
columns = ['duration','radiant_win','tower_status_radiant',
           'tower_status_dire','barracks_status_radiant','barracks_status_dire']
df.drop(columns, inplace=True, axis=1)
X_train = df
kf = KFold(n_splits=5, random_state=1, shuffle=True)

#3 find best config for size forest
n_est_max_accuracy = 0
for i in range(10,101,10):
    rnd = GradientBoostingClassifier(learning_rate=1.0, n_estimators=i, random_state=241)
    start_time = datetime.datetime.now()
    scores = cross_val_score(rnd, X_train, y_train, cv=kf, scoring='roc_auc')
    print('i=', i, scores)
    print 'Time elapsed:', datetime.datetime.now() - start_time
    roc_auc = round(scores.mean(), 5)
    print("Accuracy: %0.5f" % roc_auc)
    if roc_auc > n_est_max_accuracy:
        n_est_max_accuracy = roc_auc

print("Max accuracy: %0.5f" % n_est_max_accuracy)

# Log reg
# 1 Check C coef for LogisticRegression
learning_c = [1, 0.1, 0.01, 0.001, 1e-10, 1e-100]
kf = KFold(n_splits=5, random_state=1, shuffle=True)

roc_arr_value = np.array([])
for i in learning_c:
    roc_arr_value = np.append(roc_arr_value, calc_roc_auc(X_train, y_train, i))

c = learning_c[np.argmax(roc_arr_value, axis=0)]
calc_roc_auc_scale(X_train, y_train, c)

# 2 Calc with remove categ features
categorial_features = ['lobby_type', 'r1_hero', 'r2_hero', 'r3_hero', 'r4_hero', 'r5_hero',
                       'd1_hero', 'd2_hero', 'd3_hero', 'd4_hero', 'd5_hero']
df.drop(categorial_features, inplace=True, axis=1)
X_train = df
calc_roc_auc_scale(X_train, y_train, c)

# 3 Calc uniq heroes
hero_ = ['match_id', 'r1_hero', 'r2_hero', 'r3_hero', 'r4_hero', 'r5_hero',
         'd1_hero', 'd2_hero', 'd3_hero', 'd4_hero', 'd5_hero']
df_heroes = pd.read_csv('./features.csv', usecols=hero_, index_col='match_id')
value_uniq = calc_uniq_hero(df_heroes, hero_)
print('Unique elements in column:')
print(value_uniq)

#4 Calc after add wordsBag
wordsBag = np.hstack((X_train, formBag(df_heroes, value_uniq)))
calc_roc_auc_scale(wordsBag, y_train, c)

#5 Find max and min for logistic reg with bag
X_test = read_csv_with_scale_bag('./features_test.csv')
clf = LogisticRegression(penalty='l2',  random_state=241)
scaler = StandardScaler()
x = scaler.fit_transform(wordsBag)
X_test = scaler.transform(X_test)
clf.fit(x, y_train)

print min(clf.predict_proba(X_test)[:, 1])
print max(clf.predict_proba(X_test)[:, 1])