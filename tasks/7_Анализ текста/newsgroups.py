from sklearn.svm import SVC
import numpy as np
import pandas as pd
from sklearn import datasets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV

#Загрузите выборку. Массив с текстами будет находиться в поле newsgroups.data, номер класса — в поле newsgroups.target.
newsgroups = datasets.fetch_20newsgroups(
                    subset='all', 
                    categories=['alt.atheism', 'sci.space']
             )
y = newsgroups.target
X = newsgroups.data
#Преобразование обучающей выборки нужно делать с помощью функции fit_transform
vectorizer = TfidfVectorizer()
Z = vectorizer.fit_transform(X)
#какому слову соответствует i-й признак
feature_mapping = vectorizer.get_feature_names()
print (feature_mapping[2])
#Подбор параметров удобно делать с помощью класса:
grid = {'C': np.power(10.0, np.arange(-5, 6))}
cv = KFold(n_splits=5, shuffle=True, random_state=241)
clf = SVC(kernel='linear', random_state=241)
#Первым аргументом в GridSearchCV передается классификатор, 
#для которого будут подбираться значения параметров, вторым — словарь (dict), 
#задающий сетку параметров для перебора.
gs = GridSearchCV(clf, grid, scoring='accuracy', cv=cv)
gs.fit(Z, y)
c13 = gs.best_params_
print(c13)
#SVM with the optimal parameter
svm=SVC(C=1, kernel='linear', random_state=241)
svm.fit(Z, y)

coef=svm.coef_
words_=pd.DataFrame(coef.toarray(), columns=feature_mapping)
print (words_)
res_words = np.argsort(np.abs(np.asarray(svm.coef_.todense())).reshape(-1))[-10:]
print (res_words)
for len in range(10):
	print(vectorizer.get_feature_names()[res_words[len]])


