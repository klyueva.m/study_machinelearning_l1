import pandas
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

# Загрузите обучающую и тестовую выборки из файлов perceptron-train.csv и perceptron-test.csv. Целевая переменная записана в первом столбце, признаки — во втором и третьем. Файлы находятся в рабочей директории.
df_train = pandas.read_csv('perceptron-train.csv', header=None, names=["1", "2", "3"])
train_classes = df_train["1"]
train_observations = df_train.drop("1", axis=1)
df_test = pandas.read_csv('perceptron-test.csv', header=None, names=["1", "2", "3"])
test_classes = df_test["1"]
test_observations = df_test.drop("1", axis=1)

# Обучите персептрон со стандартными параметрами и random_state=241.
clf = Perceptron(random_state=241, max_iter=5, tol=None)
clf.fit(train_observations, train_classes)
test_classes_predicted_no_scaling = clf.predict(test_observations)

# Подсчитайте качество (долю правильно классифицированных объектов, accuracy) полученного классификатора на тестовой выборке.
accuracy_no_scaling = accuracy_score(test_classes, test_classes_predicted_no_scaling, normalize=True)

# Нормализуйте обучающую и тестовую выборку с помощью класса StandardScaler.
scaler = StandardScaler()
train_observations_scaled = scaler.fit_transform(train_observations)
test_observations_scaled = scaler.transform(test_observations)

# Обучите персептрон на новых выборках. Найдите долю правильных ответов на тестовой выборке.
clf.fit(train_observations_scaled, train_classes)
test_classes_predicted_scaling = clf.predict(test_observations_scaled)

accuracy_scaling = accuracy_score(test_classes, test_classes_predicted_scaling, normalize=True)

# Найдите разность между качеством на тестовой выборке после нормализации и качеством до нее. Это число и будет ответом на задание.
diffval = accuracy_scaling - accuracy_no_scaling

# Если ответом является нецелое число, то целую и дробную часть необходимо разграничивать точкой, например, 0.421. При необходимости округляйте дробную часть до трех знаков.
file_answer = open("answer.txt", "w")
file_answer.write(repr(round(diffval, 3)))
file_answer.close()