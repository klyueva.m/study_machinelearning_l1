import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier

data = pd.read_csv('titanic.csv', index_col='PassengerId')
data_labels = ['Pclass', 'Fare', 'Age', 'Sex']
data_sample = data[['Pclass', 'Fare', 'Age', 'Sex', 'Survived']]
data_sample = data_sample.dropna()
data_sample['Sex'] = data_sample['Sex'].map(lambda Sex: 1 if Sex == 'male' else 0)
data_target = data_sample['Survived']
data_sample_labels = data_sample[data_labels]

clf = DecisionTreeClassifier(random_state=241)
clf.fit(np.array(data_sample_labels.values), np.array(data_target.values))

importances = pd.Series(clf.feature_importances_, index = data_labels)

print (clf)
print (importances)
print (data_sample_labels)