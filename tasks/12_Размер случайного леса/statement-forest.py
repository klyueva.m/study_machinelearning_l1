import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from sklearn.metrics import make_scorer
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
#==============================================================================
# Загрузите данные из файла abalone.csv. Это датасет, в котором требуется 
#предсказать возраст ракушки (число колец) по физическим измерениям
#==============================================================================
data_train=pd.read_csv('abalone.csv')
#Преобразуйте признак Sex в числовой: значение F должно перейти в -1, I — в 0, M — в 1. 
data_train['Sex'] = data_train['Sex'].map(lambda x: 1 if x == 'M' else (-1 if x == 'F' else 0))
#Разделите содержимое файлов на признаки и целевую переменную.
#В последнем столбце записана целевая переменная, в остальных — признаки.
train_X=data_train.iloc[:,:-1].values
train_Y=data_train.iloc[:,-1:].values.ravel()
#В качестве меры качества воспользуйтесь коэффициентом детерминации (sklearn.metrics.r2_score).
ms=make_scorer(r2_score)
#инициализируем словарь
validationTest={}
#Конструктор кросс-валидации
kf = KFold(random_state=1,shuffle=True)
#Обучите случайный лес с различным числом деревьев: от 1 до 50
for i in range(50):
	i_estimators=i+1
	clf = RandomForestRegressor(i_estimators, random_state=1)
	#обучать алгоритм не нужно, мы лишь проверяем его качество
	#Оценка алгоритма
	scores = cross_val_score(clf, train_X, train_Y, scoring=ms ,cv=kf)
	#берем среднее значение оценки
	val=scores.mean()
	#проверка качества по целевому показателю
	if val>0.52:
		validationTest[i_estimators]=val

#==============================================================================
# Определите, при каком минимальном количестве деревьев случайный лес показывает качество
# на кросс-валидации выше 0.52. Это количество и будет ответом на задание.
#==============================================================================
#формируем датасет для сортировки
validationTestDataFrame=pd.DataFrame.from_dict(validationTest, orient='index')#получаем из словаря датасет
validationTestDataFrame.index.name = 'k'
validationTestDataFrame.columns =['Scores']
validationTestDataFrame.sort_index(ascending=True,inplace=True)
print('Определите, при каком минимальном количестве деревьев случайный лес показывает качество ');
print(validationTestDataFrame.head(5))