import pandas as pd
data = pd.read_csv('titanic.csv', index_col='PassengerId')

#Какое количество мужчин и женщин ехало на корабле?
#Мое решение
count_male = data[data.Sex == 'male'].Sex.count()
print ('count_male', count_male)
count_female = data[data.Sex == 'female'].Sex.count()
print ('count_female', count_female)
#Решение курса
count_sex = data.Sex.value_counts()
print (count_sex)

#Какой части пассажиров удалось выжить?
count_survived = data.Survived.count()
print (count_survived)
count_survived_1 = data[data.Survived == 1].Survived.count()
print (count_survived_1)
survived_1 = round(((count_survived_1 * 100)/count_survived), 2)
print (survived_1)

#Какую долю пассажиры первого класса составляли среди всех пассажиров?
count_Pclass = data.Pclass.count()
print (count_Pclass)
count_Pclass_1 = data[data.Pclass == 1].Pclass.count()
print (count_Pclass_1)

pclass_1 = round(((count_Pclass_1 * 100)/count_Pclass), 2)
print (pclass_1)

#mean and median
mean_age = data.Age.mean()
print ('mean_age', mean_age)
median_age = data.Age.median()
print ('median_age', median_age)

#Корреляция Пирсона
print (data[['SibSp','Parch']].corr(method = 'pearson'))

#Самое популярное женское имя(First Name)
#male
female_name = data[data.Sex == 'female'].Name
print (female_name)
female_name_split = female_name.str.split(' ', expand=True)
female_name_split.columns=['_0', '_1', '_2', '_3', '_4', '_5', '6', '7', '8', '9', '10', '11', '12', '13']
print (female_name_split)
miss_name = female_name_split[female_name_split._1 == 'Miss.']._2.value_counts()
mrs_name = female_name_split[female_name_split._1 == 'Mrs.']

print (miss_name)
print (mrs_name.srt.startswith('('))

