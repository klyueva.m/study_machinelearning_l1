import numpy as np
import pandas as pd
from sklearn.datasets import load_boston
from sklearn.model_selection import KFold, cross_val_score
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import scale

dataBoston = load_boston()
X = scale(dataBoston['data'])
y = dataBoston['target']
kf = KFold(n_splits=5, random_state=42, shuffle=True)
value = np.linspace(1, 10, 200)
knn = KNeighborsRegressor(n_neighbors = 5, weights='distance', metric='minkowski', metric_params=value)
quality = cross_val_score(knn, X, y, cv=kf, scoring='neg_mean_squared_error')

#data = np.genfromtxt('wine.data', delimiter=',')
#y = data[:,0]
#x = data[:,1:]
#kf = KFold(n_splits=5, random_state=42, shuffle=True)
#max_quality_mean = 0
#max_qm_neighbors = 0
#for k in range(1, 50):
#	knn = KNeighborsClassifier(n_neighbors = k)
#	quality_mean = np.mean(quality)
#	if (quality_mean>max_quality_mean):
#		max_quality_mean=quality_mean
#		max_qm_neighbors = k
#print(max_qm_neighbors, max_quality_mean)
