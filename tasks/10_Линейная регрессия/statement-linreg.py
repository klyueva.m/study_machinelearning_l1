import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
from scipy.sparse import hstack
from sklearn.linear_model import Ridge
#==============================================================================
# Загрузите данные об описаниях вакансий и соответствующих годовых зарплатах 
# из файла salary-train.csv
#==============================================================================
data=pd.read_csv('salary-train.csv')
X_test=pd.read_csv('salary-test-mini.csv')
print(data)
X_train = data[['FullDescription', 'LocationNormalized', 'ContractTime']]
print(X_train)
y_train = data.SalaryNormalized
print(y_train)
#==============================================================================
#Проведите предобработку:
#==============================================================================
#Приведите тексты к нижнему регистру
X_train.FullDescription = X_train.FullDescription.str.lower()
X_train.LocationNormalized = X_train.LocationNormalized.str.lower()
X_train.ContractTime = X_train.ContractTime.str.lower()
print(X_train)
#Замените все, кроме букв и цифр, на пробелы
X_train.FullDescription = X_train.FullDescription.replace('[^a-zA-Z0-9]', ' ', regex = True)
X_train.LocationNormalized = X_train.LocationNormalized.replace('[^a-zA-Z0-9]', ' ', regex = True)
X_train.ContractTime = X_train.ContractTime.str.replace('[^a-zA-Z0-9]', ' ', regex = True)
#Замените пропуски в столбцах LocationNormalized и ContractTime на специальную строку 'nan'.
X_train.LocationNormalized = X_train.LocationNormalized.fillna('nan')
X_train.ContractTime = X_train.ContractTime.fillna('nan')
print (X_train)
#Примените TfidfVectorizer для преобразования текстов в векторы признаков. 
#Оставьте только те слова, которые встречаются хотя бы в 5 объектах (параметр min_df у TfidfVectorizer).
vectorizer = TfidfVectorizer(min_df=5)
X_train_vec = vectorizer.fit_transform(X_train['FullDescription'])
X_test_vec = vectorizer.transform(X_test['FullDescription'])
#Примените DictVectorizer для получения one-hot-кодирования признаков LocationNormalized и ContractTime
enc = DictVectorizer()
X_train_categ = enc.fit_transform(X_train[['LocationNormalized', 'ContractTime']].to_dict('records'))
X_test_categ = enc.transform(X_test[['LocationNormalized', 'ContractTime']].to_dict('records'))
#Объедините все полученные признаки в одну матрицу "объекты-признаки". 
X_for_train = hstack([X_train_vec, X_train_categ])
X_for_test = hstack([X_test_vec, X_test_categ])
#==============================================================================
#Обучите гребневую регрессию с параметрами alpha=1 и random_state=241. 
#Целевая переменная записана в столбце SalaryNormalized.
#==============================================================================
ridge = Ridge(alpha=1, random_state=241)
q = ridge.fit(X_for_train, y_train)
print (q)
#==============================================================================
# Постройте прогнозы для двух примеров из файла salary-test-mini.csv.
#==============================================================================
predict=ridge.predict(X_for_test)

print('Прогноз по зарплате')
print (' '.join(map(lambda x: str(round(x,2)), predict)))
