import numpy as np
import pandas as pd
from sklearn.svm import SVC

#Загрузите выборку из файла svm-data.csv.
svmData = pd.read_csv('svm-data.csv', header=None, names=["1", "2", "3"])
#В нем записана двумерная выборка (целевая переменная указана в первом столбце, признаки — во втором и третьем).
target_data = svmData["1"]
feature_data = svmData.drop("1", axis=1)
print(target_data)
print(feature_data)
model = SVC(C=100000, random_state=241, kernel='linear')
model.fit(feature_data, target_data)
print(model.support_vectors_)
print(model.support_ + 1)

