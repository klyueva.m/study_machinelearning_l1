import numpy as np
import pandas as pd
from sklearn.model_selection import KFold, cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import scale

data = np.genfromtxt('wine.data', delimiter=',')
y = data[:,0]
x = data[:,1:]
kf = KFold(n_splits=5, random_state=42, shuffle=True)
max_quality_mean = 0
max_qm_neighbors = 0
for k in range(1, 50):
	knn = KNeighborsClassifier(n_neighbors = k)
	quality = cross_val_score(knn, scale(x), y, cv=kf, scoring='accuracy')
	quality_mean = np.mean(quality)
	if (quality_mean>max_quality_mean):
		max_quality_mean=quality_mean
		max_qm_neighbors = k
print(max_qm_neighbors, max_quality_mean)
